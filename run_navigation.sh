#!/bin/bash
#
# start the onboard navigation, including:
# - AprilTag 
# - Voxblox

source ros_environment.sh

roslaunch voxl_navigation voxl_nav.launch
