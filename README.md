# VOXL MAVROS Test w/Voxblox

This projects hosts the catkin_ws used to compile and run mavros nodes as well as the voxblox package. It is meant to be built and run inside the roskinetic-xenial expanded docker image. Eventually, the mavros package can be excluded but for now this is easier. 

For more detailed instructions on using this, see https://docs.modalai.com/mavros/ @todo UPDATE with better documentation link

The repo was forked from https://gitlab.com/voxl-public/mavros_test @todo UPDATE LINK

## PROJECT DEPENDENCIES
### docker
- First, you need to get the roskinetic docker images [here](https://gitlab.com/LucasWaelti/roskinetic-docker/-/tree/expand)
    - Follow the steps [here](https://gitlab.com/LucasWaelti/roskinetic-docker/-/tree/expand#prerequisites) to install - all 4 need to be installed, but we will only be using the expanded image

### voxl-hal3-tof-ros
- Clone my fork [here](https://gitlab.com/matt.turi/voxl-hal3-tof-cam-ros)

### voxl-qvio-server
- Clone my fork (trials branch) from [here](https://gitlab.com/matt.turi/voxl-qvio-server/-/tree/trials)
- Build following readme documentation (my fork makes the `voxl-inspect-qvio` exectutable also publish the necessary vio data for voxblox)

### voxl-imu-server
- clone and build following readme [here](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-imu-server)

### voxl-camera-server
- clone an build following readme [here](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-camera-server)

### voxblox on host pc
- Follow steps to build [here](https://voxblox.readthedocs.io/en/latest/pages/Installation.html)
    - On host PC, need the voxblox_rviz plugin to interpret and visually publish the mesh that we will produce

## SETUP
- clone my fork of the mavros_test repo
- cd into mavros_test
- run `./utils.pull_voxblox.sh` to pull the voxblox package in
- run `git submodule update --init --recursive` to initialize the apriltag submodules and my forked [voxblox_interface](https://gitlab.com/matt.turi/voxblox_interface)
- run `git submodule update --remote --recursive` to fully clone the submodules into the `/catkin_ws/src` directory

- Now set a linked storage layout in voxl, linking data partition to mavros_test:
    - `adb shell`, then `bash`
    - `mkdir -p /data/ros_storage/mavros_test`
    - `ln -s /data/ros_storage/mavros_test ./mavros_test`

- Now push the source code:
(NEED TO FIX UTILS SCRIPT @todo sometimes utils don't get pushed, may manually have to do that for now)
    - `./utils/push_to_voxl.sh`

## BUILDING CATKIN_WS
- Run docker image (interactive mode) in mavros_test dir:
    - yocto (home/root/mavros_test) `./start_docker.sh -i`

- Build project(inside docker image exp):
    - `./utils/init_workspace.sh`
    - `./utils/build.sh`

- This will take a while, I had to change the flags and decrease the load to prevent overheating
- When finished, if all packages built succesfully, it should be ready to run

## RUNNING ON VOXL
- In order to actually publish a mesh, you need to have:
    - hal3-tof-cam-ros running, publishing a pointcloud
        - (Needs a roscore started as well)
    - voxl-qvio-server running with voxl-inspect-qvio
    - voxl-imu-server running
    - voxl-camera-server running
    - ONCE THESE ARE ALL RUNNING, then start the modified voxl-test-qvio which will publish 6 DOF vio data over ros
- If everything started correctly, and you have updated your IP addresses in the ros_environment.sh script, start the voxblox package
    - `./run_mavros_apriltag_voxblox.sh`
- To view published mesh, go into where you installed voxblox on host pc. Souce the `setup.bash` script in `/catkin_ws/devel` directory, export correct ROS_IP, and run `rviz`. The data is published in the fixed frame "world", and you just need to add the voxblox_node -> voxblox_mesh topic (not the voxblox_interface)
