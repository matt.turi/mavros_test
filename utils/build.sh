#!/bin/bash
#
# builds everything in isolation and installs
#
# Modal AI Inc. 2019
# author: james@modalai.com
# modifed: Matt Turi (January 2021)

set -e

cd catkin_ws

# load main ros environment
if [ -f /opt/ros/melodic/setup.bash ]; then
    source /opt/ros/melodic/setup.bash
elif [ -f /opt/ros/kinetic/setup.bash ]; then
    source /opt/ros/kinetic/setup.bash
elif [ -f /opt/ros/indiego/setup.bash ]; then
    source /opt/ros/indigo/setup.bash
fi

if [[ $# > 0 ]]; then
	catkin_make_isolated -j4 -l2 --install --only-pkg-with-deps $@
else
	catkin_make_isolated -j4 -l2 --install
fi 


