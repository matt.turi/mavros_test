/**
 * Note about reference frames: 
 *  - mavros: works in ENU and converts to NED 
 *  - PX4: uses NED 
 *  Actually, it seems that directly thinking in EWU (ROS default)
 *  works fine. The drone simply starts at yaw=pi/2. 
 *  
 * All targets are computed based on the EWU convention. 
 */

#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float64.h>
#include <std_srvs/SetBool.h>
#include <std_srvs/Trigger.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include <tf2/LinearMath/Quaternion.h>

#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/CommandTOL.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/State.h>
#include <mavros_msgs/PositionTarget.h>

#include <math.h>

#define PI  3.14159265358979323846264338327950

#define PRE_LANDING_ALTITUDE    0.4f
#define LANDING_ALTITUDE        0.1f
#define YAW_OFFSET              -PI/2.0 // Offset at takeoff due to ENU/NWU mismatch
#define RATE                    20  // loop rate hz
#define PRECISION_RADIUS        0.15 

static bool flight_on = false; 
static bool landing = false; 

static mavros_msgs::State current_state;
static mavros_msgs::PositionTarget target; 
static geometry_msgs::Vector3Stamped speed; 
static geometry_msgs::Twist current_pose;

static ros::Subscriber state_sub;
static ros::Subscriber local_pose_sub;
static ros::Subscriber target_sub;
static ros::Subscriber speed_sub;

static ros::Publisher target_local_pub;
static ros::Publisher d_to_tar_pub; 
static ros::Publisher target_reached_pub; 

static ros::ServiceClient arming_client;
static ros::ServiceClient land_client;
static ros::ServiceClient set_mode_client;
static ros::ServiceServer takeoff_srv;
static ros::ServiceServer land_srv;

void get_fcu(ros::Rate rate){
    while(ros::ok() && current_state.connected){
        ros::spinOnce();
        rate.sleep();
        ROS_INFO_ONCE("\rconnecting to FCT...");
    }
}

void get_home_pose(mavros_msgs::PositionTarget &position_home){
    position_home.coordinate_frame = mavros_msgs::PositionTarget::FRAME_LOCAL_NED;
    position_home.type_mask = mavros_msgs::PositionTarget::IGNORE_VX |
                              mavros_msgs::PositionTarget::IGNORE_VY |
                              mavros_msgs::PositionTarget::IGNORE_VZ |
                              mavros_msgs::PositionTarget::IGNORE_AFX |
                              mavros_msgs::PositionTarget::IGNORE_AFY |
                              mavros_msgs::PositionTarget::IGNORE_AFZ |
                              mavros_msgs::PositionTarget::IGNORE_YAW_RATE;
    position_home.position.x = 0;
    position_home.position.y = 0;
    position_home.position.z = PRE_LANDING_ALTITUDE;
    position_home.velocity.x = 0;
    position_home.velocity.y = 0;
    position_home.velocity.z = 0;
    position_home.acceleration_or_force.x = 0;
    position_home.acceleration_or_force.y = 0;
    position_home.acceleration_or_force.z = 0;

    position_home.yaw = 0.0 + YAW_OFFSET; 
}

void spin_target(ros::Rate &rate){
    target_local_pub.publish(target);
    ros::spinOnce();
    rate.sleep();
}

/**
 * Arm/disarm the drone. 
 * @arg: arm (bool) 
 * @return: (1) success, (0) failed
 */
int arm(ros::ServiceClient& arming_client, bool arm=true){
    // Instantiate arm request 
    mavros_msgs::CommandBool arm_cmd;
    arm_cmd.request.value = arm;
    // Send arm request 
    if( arming_client.call(arm_cmd) &&
        arm_cmd.response.success){
        if(arm)
            ROS_INFO("Vehicle armed");
        else
            ROS_INFO("Vehicle disarmed");
        return 1;
    }else{
        if(arm)
            ROS_ERROR("Arming request failed");
        else
            ROS_ERROR("Disarming request failed");
        return 0; 
    }
}

int set_offboard(ros::ServiceClient &set_mode_client){
    mavros_msgs::SetMode offb_set_mode;
    offb_set_mode.request.custom_mode = "OFFBOARD";
    // Send mode request 
    if(set_mode_client.call(offb_set_mode) &&
        offb_set_mode.response.mode_sent){
        ROS_INFO("Offboard enabled");
        return 1; 
    }else{
        ROS_ERROR("Offboard not enabled");
        return 0; 
    }
}

double distance_to_target(mavros_msgs::PositionTarget &t){
    return sqrt(pow(t.position.x-current_pose.linear.x,2)
        + pow(t.position.y-current_pose.linear.y,2)
        + pow(t.position.z-current_pose.linear.z,2));
}
double distance_to_target(){
    return distance_to_target(target);
}

void advertise_distance(){
    std_msgs::Float64 msg; 
    msg.data = distance_to_target();
    d_to_tar_pub.publish(msg);
}

bool target_reached(mavros_msgs::PositionTarget &t){
    return (distance_to_target(t) < PRECISION_RADIUS);
}
bool target_reached(){
    return (distance_to_target(target) < PRECISION_RADIUS);
}

void advertise_target_reached(){
    std_msgs::Bool msg; 
    msg.data = target_reached();
    target_reached_pub.publish(msg);
}

/**
 * NOTE: function unused (handled by the mission) 
 */
void go_home(const ros::Publisher &target_local_pub, ros::Rate &rate){
    get_home_pose(target); 
    for(int i=0; i<2; i++){
        if(i==0) target.position.z = PRE_LANDING_ALTITUDE;
        else target.position.z = LANDING_ALTITUDE;
        while(!target_reached()){
            advertise_distance();
            advertise_target_reached();
            spin_target(rate);
        }
    }
}

// ------------------ Callbacks ------------------

void state_cb(const mavros_msgs::State::ConstPtr& msg)
{
    current_state = *msg;
}

void pose_cb(const geometry_msgs::PoseStamped::ConstPtr &msg){
    // Express in NWU 
    current_pose.linear.x = msg->pose.position.x; 
    current_pose.linear.y = msg->pose.position.y; 
    current_pose.linear.z = msg->pose.position.z; 

    tf2::Quaternion q; 
    q.setW(msg->pose.orientation.w);
    q.setX(msg->pose.orientation.x);
    q.setY(msg->pose.orientation.y);
    q.setZ(msg->pose.orientation.z);

    tf2::Matrix3x3 m; m.setRotation(q);
    tf2Scalar roll, pitch, yaw;
    m.getRPY(roll, pitch, yaw);

    current_pose.angular.x = roll; 
    current_pose.angular.y = pitch; 
    current_pose.angular.z = yaw; 
}

void target_callback(const geometry_msgs::Twist::ConstPtr &msg){
    target.header.stamp = ros::Time::now();

    target.coordinate_frame = mavros_msgs::PositionTarget::FRAME_LOCAL_NED;

    // Set NWU command 
    target.position.x = msg->linear.x;
    target.position.y = msg->linear.y;
    target.position.z = msg->linear.z;

    // If speed command is too old, ignore it 
    if(target.header.stamp.toSec() - speed.header.stamp.toSec() > 0.2){
        target.type_mask =  mavros_msgs::PositionTarget::IGNORE_VX |
                            mavros_msgs::PositionTarget::IGNORE_VY |
                            mavros_msgs::PositionTarget::IGNORE_VZ |
                            mavros_msgs::PositionTarget::IGNORE_AFX |
                            mavros_msgs::PositionTarget::IGNORE_AFY |
                            mavros_msgs::PositionTarget::IGNORE_AFZ |
                            mavros_msgs::PositionTarget::IGNORE_YAW_RATE;
        target.velocity.x = 0;
        target.velocity.y = 0;
        target.velocity.z = 0;
    }
    // Otherwise, apply it 
    else{
        target.type_mask =  mavros_msgs::PositionTarget::IGNORE_AFX |
                            mavros_msgs::PositionTarget::IGNORE_AFY |
                            mavros_msgs::PositionTarget::IGNORE_AFZ |
                            mavros_msgs::PositionTarget::IGNORE_YAW_RATE;
        target.velocity.x = speed.vector.x;
        target.velocity.y = speed.vector.y;
        target.velocity.z = speed.vector.z;
    }
    target.acceleration_or_force.x = 0;
    target.acceleration_or_force.y = 0;
    target.acceleration_or_force.z = 0;

    target.yaw = msg->angular.z; 
}

void speed_callback(const geometry_msgs::Vector3Stamped::ConstPtr &msg){
    speed = *msg; 
}

bool takeoff_callback(std_srvs::Trigger::Request  &req,
                        std_srvs::Trigger::Response &res){
    // Send OFFBOARD mode request and arm the drone
    if(set_offboard(set_mode_client) && arm(arming_client, true)){
        res.message = "Taking off"; 
        res.success = true; 
        flight_on = true; 
    }else{
        res.message = "Cannot take off"; 
        res.success = false; 
        flight_on = false;  
    }
    return true;
}

bool land_callback(std_srvs::Trigger::Request  &req,
                    std_srvs::Trigger::Response &res){    
    res.message = "Landing"; 
    res.success = true; 
    landing = true; 
    return true; 
}

// ------------------ Callbacks (end) ------------------

int main(int argc, char **argv)
{
    ros::init(argc, argv, "mavros_interface");
    ros::NodeHandle nh;

    ROS_INFO("Mavros interface started"); 

    // Set up Mavros subscribers, publishers and clients 
    state_sub           = nh.subscribe<mavros_msgs::State>
                                        ("mavros/state", 10, state_cb);
    local_pose_sub      = nh.subscribe<geometry_msgs::PoseStamped>
                                        ("mavros/local_position/pose", 10, pose_cb);
    //ros::Publisher local_pos_pub        = nh.advertise<geometry_msgs::PoseStamped>
    //                                    ("mavros/setpoint_position/local", 10);
    target_local_pub    = nh.advertise<mavros_msgs::PositionTarget>
                                        ("mavros/setpoint_raw/local", 10);
    arming_client       = nh.serviceClient<mavros_msgs::CommandBool>
                                        ("mavros/cmd/arming");
    land_client         = nh.serviceClient<mavros_msgs::CommandTOL>
                                        ("mavros/cmd/land");
    set_mode_client     = nh.serviceClient<mavros_msgs::SetMode>
                                        ("mavros/set_mode");

    // Set up interface subscribers and services
    target_sub          = nh.subscribe<geometry_msgs::Twist>
                                        ("/v_nav/target", 10, target_callback);
    speed_sub           = nh.subscribe<geometry_msgs::Vector3Stamped>
                                        ("/v_nav/speed",2,speed_callback);
    takeoff_srv         = nh.advertiseService
                                        ("/v_nav/takeoff", takeoff_callback);
    land_srv            = nh.advertiseService
                                        ("/v_nav/land", land_callback);
    d_to_tar_pub        = nh.advertise<std_msgs::Float64>
                                        ("/v_nav/distance_target", 2);
    target_reached_pub  = nh.advertise<std_msgs::Bool>
                                        ("/v_nav/target_reached", 2);

    // Note: the setpoint publishing rate MUST be faster than 2Hz
    ros::Rate rate(RATE);

    // Initialize pose
    current_pose.linear.x = 0.0; 
    current_pose.linear.y = 0.0; 
    current_pose.linear.z = 0.0; 
    current_pose.angular.x = 0.0; 
    current_pose.angular.y = 0.0;
    current_pose.angular.z = 0.0;

    // Wait for FCU connection
    get_fcu(rate);

START: 

    // Reset state 
    landing = false; 
    flight_on = false; 

    // Define home position 
    get_home_pose(target); 
    target.position.x = current_pose.linear.x; 
    target.position.y = current_pose.linear.y; 
    target.yaw = current_pose.angular.z; 

    // Initialize speed command
    speed.header.stamp = ros::Time::now(); 
    speed.vector.x = 0.0; 
    speed.vector.y = 0.0; 
    speed.vector.z = 0.0; 

    // Send setpoints to enable OFFBOARD mode and wait for takeoff request 
    ROS_INFO("Waiting for takeoff request...");
    while(ros::ok() && !flight_on){
        advertise_distance();
        advertise_target_reached();
        spin_target(rate);
    }

RECOVER:

    // Wait until the drone is armed and in OFFBOARD mode
    while(ros::ok()){
        target_local_pub.publish(target);
        ros::spinOnce();
        rate.sleep();
        if(current_state.mode == "OFFBOARD" && current_state.armed) break;
    }ROS_INFO("Drone armed and in OFFBOARD mode");

    // Main loop - continuously publish current target 
    while(ros::ok() && !landing){
        // Fail safe 
        if(current_state.mode != "OFFBOARD" || !current_state.armed){
            ROS_ERROR("Drone no longer in OFFBOARD mode or disarmed, recovering");
            goto RECOVER;
        }
        advertise_distance();
        advertise_target_reached();
        spin_target(rate);
    }

    // Land 
    mavros_msgs::CommandTOL land_cmd;
    land_cmd.request.yaw = current_pose.angular.z; // Current orientation instead of 0 
    land_cmd.request.latitude = 0;
    land_cmd.request.longitude = 0;
    land_cmd.request.altitude = 0;
    while (!(land_client.call(land_cmd) &&
            land_cmd.response.success))
        ROS_INFO_ONCE("Trying to land...");
    ROS_INFO("Landed");

    // Disarm the drone
    //arm(arming_client, false); // seems it should not be called

    ROS_INFO("Mavros interface ready for new flight");
    goto START; 

    return 0;
}
